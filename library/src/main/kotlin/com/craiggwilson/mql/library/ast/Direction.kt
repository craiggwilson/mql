package com.craiggwilson.mql.library.ast

enum class Direction {
    ASCENDING,
    DESCENDING
}
