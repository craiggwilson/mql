package com.craiggwilson.mql.gui

import tornadofx.launch

fun main(args: Array<String>) = launch<MainApp>()
